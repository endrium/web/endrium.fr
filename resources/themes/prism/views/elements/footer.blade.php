<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h4>{{ trans('theme::prism.footer.about') }}</h4>

            <p style="font-size: 14px;">{!! theme_config('footer_description') !!}</p>
        </div>

        <div class="col-md-4 social">
<!--             <h4>{{ trans('theme::prism.footer.social') }}</h4> -->
            <h4>Rejoins la communauté</h4>
            <p style="font-size: 12px;">Sur notre discord tu pourra retrouver tous les joueurs d'Endrium.
            Il y a divers salons mis en place afin que votre aventure se déroule dans les meilleures conditions possible, vous pouvez y partager même votre pub lors d'un live ou d'une vidéo sur notre serveur !</p>

<!--             <ul class="list-inline">
                @foreach(['twitter', 'youtube', 'discord', 'steam', 'teamspeak', 'instagram'] as $social)
                    @if($socialLink = theme_config("footer_social_{$social}"))
                        <li class="list-inline-item">
                            <a href="{{ $socialLink }}" target="_blank" rel="noreferrer noopener" title="{{ trans('theme::prism.links.'.$social) }}"><i style="font-size: 2.2em;" class="fab fa-{{ $social }} fa-2x"></i></a>
                        </li>
                    @endif
                @endforeach
            </ul> -->

            <a href="https://discord.gg/rvDJbqm" target="_blank" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-discord fa-2x"></i> Discord</a>

        </div>

        <div class="col-md-4 links">
            <!-- <h4>{{ trans('theme::prism.footer.links') }}</h4> -->
            <h4>Le launcher</h4>

            <a href="https://alexm.best/launcher/releases/win/launcher-endrium-2.2.0.exe" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-windows fa-2x"></i> Launcher Windows</a>
            <a href="https://alexm.best/launcher/releases/linux/launcher-endrium-2.2.0-x86_64.AppImage" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-linux fa-2x"></i> Launcher Linux</a>
            <a href="https://discord.gg/rvDJbqm" target="_blank" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-apple fa-2x"></i> Launcher Mac</a>

<!--             <ul class="list-unstyled">
                @foreach(theme_config('footer_links') ?? [] as $link)
                    <li>
                        <a href="{{ $link['value'] }}"><i class="fas fa-chevron-right"></i> {{ $link['name'] }}</a>
                    </li>
                @endforeach
            </ul> -->
        </div>

    </div>

    <hr>

    <div class="row footer-bottom">
        <div class="col-md-6">
            {{ setting('copyright') }}
        </div>
        <div class="col-md-6 text-right">
            @lang('messages.copyright') • 
            <a href="http://www.serveurs-minecraft.org/">Serveur Minecraft org</a>  <a href="http://www.serveur-minecraft.eu/">Serveur Minecraft eu</a>
        </div>
    </div>
</div>
