@extends('layouts.app')

@section('title', trans('messages.home'))

@section('content')
    <div class="home-background @if(theme_config('title')) background-overlay @endif mb-4" style="background: url('{{ setting('background') ? image_url(setting('background')) : 'https://via.placeholder.com/2000x500' }}') no-repeat center / cover">
        @if(theme_config('title'))
            <div class="container h-100">
                <div class="row align-items-center justify-content-center h-100">

                    <div class="col-md-6 text-center" style="margin-top: -8%;">
                        <p class="text-server">Bienvenue sur le site</p>
                        <h1 class="title-server">Endrium</h1>
                    </div>

                </div>
            </div>
        @endif
    </div>

    <div class="container">
        <div class="content-box">

            <div class="row">
                <div class="col-md-8">
                    <div class="box-page">
                
                        @foreach($posts as $post)

                            <div class="col-md-12">
                                <div class="row" style="padding: 12px;">
                                    <div class="col-md-5">
                                        <a href="{{ route('posts.show', $post->slug) }}" class="link-unstyled">
                                            @if($post->hasImage())
                                            <img src="{{ $post->imageUrl() }}" alt="{{ $post->title }}" class="img-fluid rounded">
                                        </a>
                                    </div>
                                    <div class="col-md-7">
                                        <a href="{{ route('posts.show', $post->slug) }}" class="link-unstyled">
                                            <div class="title-news p-2">{{ $post->title }}</div>
                                        </a>
                                        <p class="text-news p-2">{{ Str::limit(strip_tags($post->content), 240) }}</p>
                                        @else
                                            <div class="preview-content p-4">
                                                <h4>{{ $post->title }}</h4>
                                                {{ Str::limit(strip_tags($post->content), 240) }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        @endforeach        

                    </div>

                    <div class="alert alert-warning" role="alert">
                      D'autres nouveautés prochainement ...
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="box-page">
                        <h4 class="title-box">Rejoignez-nous sur discord</h4>
                    </div>
                    <div class="box-page">
                        <iframe src="https://discordapp.com/widget?id=706922285944012852&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" 
                        style="margin: auto;display:block;"></iframe>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
