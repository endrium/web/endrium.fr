<?php $__env->startSection('title', $ticket->subject); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <h1><?php echo e($ticket->subject); ?></h1>

        <div class="card shadow-sm mb-3">
            <div class="card-body">
             <span class="badge badge-<?php echo e($ticket->isClosed() ? 'danger' : 'success'); ?>">
                 <?php echo e($ticket->statusMessage()); ?>

             </span>
                <?php echo app('translator')->get('support::messages.tickets.status-info', ['author' => $ticket->author->name, 'category' => $ticket->category->name, 'date' => format_date($ticket->created_at)]); ?>
            </div>
        </div>

        <?php $__currentLoopData = $ticket->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card shadow-sm mb-3">
                <div class="card-header <?php if($ticket->author->is($comment->author)): ?> text-info <?php else: ?> text-primary <?php endif; ?>">
                    <?php echo app('translator')->get('messages.comments.author', ['user' => $comment->author->name, 'date' => format_date($comment->created_at, true)]); ?>
                </div>
                <div class="card-body media">
                    <img class="d-flex mr-3 rounded" src="<?php echo e($comment->author->getAvatar()); ?>" alt="<?php echo e($comment->author->name); ?>" height="55">
                    <div class="media-body">
                        <div class="content-body">
                            <?php echo e($comment->parseContent()); ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <div class="card shadow-sm mb-3">
            <div class="card-body">
                <?php if($ticket->isClosed()): ?>
                    <p class="text-danger"><?php echo e(trans('support::messages.tickets.closed')); ?></p>
                <?php else: ?>
                    <form action="<?php echo e(route('support.tickets.comments.store', $ticket)); ?>" method="POST" class="mb-2">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label for="content"><?php echo e(trans('messages.comments.your-comment')); ?></label>
                            <textarea class="form-control <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="content" name="content" rows="4" required><?php echo e(old('content')); ?></textarea>
                        </div>

                        <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <button type="submit" class="btn btn-primary">
                            <?php echo e(trans('messages.actions.comment')); ?>

                        </button>
                    </form>

                    <form action="<?php echo e(route('support.tickets.close', $ticket)); ?>" method="POST">
                        <?php echo csrf_field(); ?>

                        <button type="submit" class="btn btn-danger">
                            <?php echo e(trans('support::messages.actions.close')); ?>

                        </button>
                    </form>
                <?php endif; ?>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/support/resources/views/tickets/show.blade.php ENDPATH**/ ?>