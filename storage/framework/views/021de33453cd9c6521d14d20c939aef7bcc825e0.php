<?php $__env->startSection('title', trans('forum::messages.title')); ?>

<?php $__env->startSection('content'); ?>

    <div class="home-background <?php if(theme_config('title')): ?> background-overlay <?php endif; ?> mb-4" style="background: url('<?php echo e(setting('background') ? image_url(setting('background')) : 'https://via.placeholder.com/2000x500'); ?>') no-repeat center / cover">
        <?php if(theme_config('title')): ?>
            <div class="container h-100">
                <div class="row align-items-center justify-content-center h-100">

                    <div class="col-md-6 text-center" style="margin-top: -8%;">
                        <p class="text-server">Echange avec la communauté</p>
                        <h1 class="title-server">Endrium</h1>
                    </div>

                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="container content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="margin-top: -18%;position: relative;background: #ffffff1f;">
                <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>"><?php echo e(trans('messages.home')); ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo e(route('forum.home')); ?>"><?php echo e(trans('forum::messages.title')); ?></a></li>

                <?php $__currentLoopData = optional($current ?? null)->getNavigationStack() ?? []; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumbLink => $breadcrumbName): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="breadcrumb-item"><a href="<?php echo e($breadcrumbLink); ?>"><?php echo e($breadcrumbName); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-9">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card mb-3">
                        <div class="card-header">
                            <h2 class="h3"><?php echo e($category->name); ?></h2>
                            <small><?php echo e($category->description); ?></small>
                        </div>

                        <div class="list-group list-group-flush">
                            <?php $__currentLoopData = $category->forums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $forum): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="list-group-item box-forum">
                                    <div class="row">
                                        <div class="col-xl-1 col-md-2 col-2 text-center">
                                            <i class="<?php echo e($forum->icon ?? 'fas fa-comments'); ?> fa-2x fa-fw forum-big-icon icon-forum"></i>
                                        </div>

                                        <div class="col-xl-8 col-md-7 col-10 pl-md-0">
                                            <h3 class="h5">
                                                <a href="<?php echo e(route('forum.show', $forum->slug)); ?>" class="titre-forum"><?php echo e($forum->name); ?></a>
                                            </h3>

                                           <p> <?php echo e($forum->description ?? ''); ?></p>
                                        </div>

                                        <div class="col-xl-3 col-md-3 d-none d-md-block">
                                            <p><?php echo e(trans_choice('forum::messages.forums.discussions-count', $forum->discussions_count)); ?></p>
                                            <p><?php echo e(trans_choice('forum::messages.discussions.posts-count', $forum->posts_count)); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            <div class="col-md-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-comments fa-fw"></i> <?php echo e(trans('forum::messages.latest.title')); ?>

                    </div>
                    <div class="list-group list-group-flush">
                        <?php $__currentLoopData = $latestPosts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="list-group-item">
                                <a href="<?php echo e(route('forum.discussions.show', $post->discussion)); ?>">
                                    <?php echo e($post->discussion->title); ?>

                                </a>

                                <br>

                                <small><?php echo e($post->author->name); ?>, <?php echo e(format_date($post->created_at)); ?></small>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-chart-bar fa-fw"></i> <?php echo e(trans('forum::messages.stats.title')); ?>

                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled mb-0">
                            <li><?php echo e(trans_choice('forum::messages.stats.discussions', $discussionsCount)); ?></li>
                            <li><?php echo e(trans_choice('forum::messages.stats.posts', $postsCount)); ?></li>
                            <li><?php echo e(trans_choice('forum::messages.stats.users', $usersCount)); ?></li>
                        </ul>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-users fa-fw"></i> <?php echo e(trans('forum::messages.online.title')); ?>

                    </div>
                    <div class="card-body">
                        <?php if(empty($onlineUsers)): ?>
                            <?php echo e(trans('forum::messages.online.none')); ?>

                        <?php else: ?>
                            <?php echo e(implode(', ', $onlineUsers)); ?>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/forum/resources/views/home.blade.php ENDPATH**/ ?>