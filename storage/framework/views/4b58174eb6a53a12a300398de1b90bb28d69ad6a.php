<?php $__env->startSection('title', $forum->name); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <?php echo $__env->make('forum::elements.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <h1 style="font-size: 26px;text-transform: uppercase;margin: 30px 0px;" class="h2"><?php echo e($forum->name); ?></h1>

        <?php if(! $forum->forums->isEmpty()): ?>
            <div class="card mb-4">
                <div class="list-group list-group-flush">
                    <?php $__currentLoopData = $forum->forums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subForum): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="list-group-item">
                            <div class="row">
                                <div class="col-xl-1 col-md-2 col-2 text-center">
                                    <i class="<?php echo e($subForum->icon ?? 'fas fa-comments'); ?> fa-2x fa-fw forum-big-icon"></i>
                                </div>

                                <div class="col-xl-8 col-md-7 col-10 pl-md-0">
                                    <h3 class="h5">
                                        <a href="<?php echo e(route('forum.show', $subForum->slug)); ?>"><?php echo e($subForum->name); ?></a>
                                    </h3>

                                    <?php echo e($subForum->description ?? ''); ?>

                                </div>

                                <div class="col-xl-3 col-md-3 d-none d-md-block">
                                    <p><?php echo e(trans_choice('forum::messages.forums.discussions-count', $subForum->discussions_count)); ?></p>
                                    <p><?php echo e(trans_choice('forum::messages.discussions.posts-count', $subForum->posts_count)); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="card mb-3">
            <div class="card-header"><?php echo e(trans('forum::messages.discussions.title')); ?></div>
            <div class="list-group list-group-flush">
                <?php $__currentLoopData = $forum->discussions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $discussion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="list-group-item box-forum">
                        <div class="row">
                            <div class="col-2 col-md-1 text-center">
                                <i class="fas fa-comment-dots fa-2x fa-fw forum-big-icon icon-forum"></i>
                            </div>

                            <div class="col-8 col-md-5 pl-md-0">
                                <a href="<?php echo e(route('forum.discussions.show', $discussion)); ?>" class="titre-forum"><?php echo e($discussion->title); ?></a>
                                <br>
                                <small><?php echo e($discussion->author->name); ?>, <?php echo e(format_date($discussion->created_at)); ?></small>
                            </div>

                            <div class="col-2">
                                <?php if($discussion->is_pinned || $discussion->is_locked): ?>
                                    <div class="float-md-right">
                                        <?php if($discussion->is_pinned): ?>
                                            <i style="color: #ff9090!important;" class="fas fa-thumbtack fa-fw text-primary" title="<?php echo e(trans('forum::messages.discussions.pinned')); ?>" data-toggle="tooltip"></i>
                                        <?php endif; ?>

                                        <?php if($discussion->is_locked): ?>
                                            <i class="fas fa-lock fa-fw text-warning" title="<?php echo e(trans('forum::messages.discussions.locked')); ?>" data-toggle="tooltip"></i>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-2 d-none d-md-block">
                                <p><?php echo e(trans_choice('forum::messages.discussions.posts-count', $discussion->posts_count)); ?></p>
                            </div>

                            <div class="col-md-2 d-none d-md-block">
                                <?php if(! $discussion->posts->isEmpty()): ?>
                                    <?php echo e($discussion->posts->first()->author->name); ?>,
                                    <small><?php echo e(format_date($discussion->posts->first()->created_at)); ?></small>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>

        <?php echo e($forum->discussions->links()); ?>


        <?php if($forum->is_locked): ?>
            <div class="alert alert-warning" role="alert">
                <i class="fas fa-lock"></i> <?php echo e(trans('forum::messages.forums.locked')); ?>

            </div>
        <?php endif; ?>

        <?php if(! $forum->is_locked || optional(auth()->user())->isAdmin()): ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', \Azuriom\Plugin\Forum\Models\Discussion::class)): ?>
                <a href="<?php echo e(route('forum.forum.discussions.create', $forum->slug)); ?>" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                    <?php echo e(trans('messages.actions.create')); ?>

                </a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/forum/resources/views/show.blade.php ENDPATH**/ ?>