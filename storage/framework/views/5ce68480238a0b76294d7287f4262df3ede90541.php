<nav class="navbar navbar-expand-md navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="<?php echo e(route('home')); ?>">
            <div class="navbar-logo">
                <img src="<?php echo e(site_logo()); ?>" alt="<?php echo e(site_name()); ?>" data-tilt data-tilt-scale="1.1">
            </div>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <?php $__currentLoopData = $navbar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($loop->index < ($loop->count / 2)): ?>
                        <?php if(!$element->isDropdown()): ?>
                            <li class="nav-item <?php if($element->isCurrent()): ?> active <?php endif; ?>">
                                <a class="nav-link" href="<?php echo e($element->getLink()); ?>" <?php if($element->new_tab): ?> target="_blank" rel="noopener" <?php endif; ?>><?php echo e($element->name); ?></a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo e($element->name); ?>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php $__currentLoopData = $element->elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $childElement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a class="dropdown-item <?php if($childElement->isCurrent()): ?> text-primary <?php endif; ?>" href="<?php echo e($childElement->getLink()); ?>" <?php if($element->new_tab): ?> target="_blank" rel="noopener" <?php endif; ?>><?php echo e($childElement->name); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <?php $__currentLoopData = $navbar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($loop->index >= ($loop->count / 2)): ?>
                        <?php if(!$element->isDropdown()): ?>
                            <li class="nav-item <?php if($element->isCurrent()): ?> active <?php endif; ?>">
                                <a class="nav-link" href="<?php echo e($element->getLink()); ?>" <?php if($element->new_tab): ?> target="_blank" rel="noopener" <?php endif; ?>><?php echo e($element->name); ?></a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown<?php echo e($element->id); ?>" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo e($element->name); ?>

                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown<?php echo e($element->id); ?>">
                                    <?php $__currentLoopData = $element->elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $childElement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <a class="dropdown-item <?php if($childElement->isCurrent()): ?> active <?php endif; ?>" href="<?php echo e($childElement->getLink()); ?>" <?php if($childElement->new_tab): ?> target="_blank" rel="noopener" <?php endif; ?>><?php echo e($childElement->name); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    </div>
</nav>

<div class="sub-navbar bg-primary py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d-flex align-items-center justify-content-center">
                <div class="media mr-lg-5 align-items-center">
                    <i class="fas fa-users fa-2x mr-2" style="font-size: 22px;"></i>
                    <div class="media-body">
                        <?php if($server && $server->isOnline()): ?>
                            <?php if(theme_config('use_play_button') !== 'on'): ?>
                                <div data-toggle="tooltip" title="<?php echo e(trans('messages.actions.copy')); ?>" data-copy-target="address" data-copied-messages="<?php echo e(implode('|', trans('theme::prism.clipboard'))); ?>">
                                    <input type="text" class="copy-address bg-primary-darker h5 text-center" id="address" style="width: <?php echo e(strlen($server->fullAddress()) / 2); ?>em" value="<?php echo e($server->fullAddress()); ?>" readonly aria-label="Address">
                                </div>
                            <?php else: ?>
                                <div>
                                    <!-- <h5 class="mb-0"><?php echo e($server->name); ?></h5> -->
                                </div>
                            <?php endif; ?>
                            <?php echo e(trans_choice('theme::prism.header.online', $server->getOnlinePlayers())); ?>

                        <?php else: ?>
                            <h5 class="mb-0"><?php echo e(trans('theme::prism.header.offline')); ?></h5>
                        <?php endif; ?>
                    </div>

                    <?php if(theme_config('use_play_button') === 'on'): ?>
                        <a href="<?php echo e(theme_config('play_button_link')); ?>" class="btn btn-outline-light btn-rounded ml-3">
                            <?php echo e(trans('theme::prism.play')); ?>

                        </a>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-6 text-center">
                <?php if(auth()->guard()->check()): ?>
                    <?php echo $__env->make('elements.notifications', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <span class="dropdown">
                        <a id="userDropdown" class="btn btn-outline-light btn-rounded dropdown-toggle my-1" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                        </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="<?php echo e(route('profile.index')); ?>">
                            <?php echo e(trans('messages.nav.profile')); ?>

                        </a>

                        <?php if(Auth::user()->hasAdminAccess()): ?>
                            <a class="dropdown-item" href="<?php echo e(route('admin.dashboard')); ?>">
                                <?php echo e(trans('messages.nav.admin')); ?>

                            </a>
                        <?php endif; ?>

                        <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <?php echo e(trans('auth.logout')); ?>

                        </a>

                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>
                    </span>
                <?php else: ?>
                    <div class="my-1 ml-lg-5 btn-group">
                        <?php if(Route::has('register')): ?>
                            <a class="btn btn-outline-light btn-rounded" href="<?php echo e(route('register')); ?>"><?php echo e(trans('auth.register')); ?></a>
                        <?php endif; ?>
                        <a class="btn btn-outline-light btn-rounded" href="<?php echo e(route('login')); ?>"><?php echo e(trans('auth.login')); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /home/endrius/www/resources/themes/prism/views/elements/navbar.blade.php ENDPATH**/ ?>