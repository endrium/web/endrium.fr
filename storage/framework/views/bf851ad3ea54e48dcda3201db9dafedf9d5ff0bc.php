

<?php $__env->startSection('title', $page->title); ?>
<?php $__env->startSection('description', $page->description); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <div class="card shadow-sm">
            <div class="card-body">
                <h1><?php echo e($page->title); ?></h1>

                <div class="card-text user-html-content">
                    <?php echo $page->content; ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/pages/show.blade.php ENDPATH**/ ?>