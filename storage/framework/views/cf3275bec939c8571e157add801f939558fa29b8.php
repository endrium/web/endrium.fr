

<?php $__env->startSection('title', trans('messages.profile.title')); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <div class="card mb-4 shadow-sm">
            <div class="card-header"><?php echo e(trans('messages.profile.title')); ?></div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-2 col-md-3 text-center">
                        <img src="<?php echo e($user->getAvatar(150)); ?>" class="rounded mb-3 img-fluid" alt="<?php echo e($user->name); ?>">

                        <h2 class="h4 mb-0">
                            <span class="badge" style="<?php echo e($user->role->getBadgeStyle()); ?>; vertical-align: middle"><?php echo e($user->role->name); ?></span>
                        </h2>
                    </div>

                    <div class="col-lx-10 col-md-9">
                        <h1><?php echo e($user->name); ?></h1>

                        <ul>
                            <li><?php echo e(trans('messages.profile.info.register', ['date' => format_date($user->created_at, true)])); ?></li>
                            <li><?php echo e(trans('messages.profile.info.money', ['money' => format_money($user->money)])); ?></li>
                            <?php if($user->game_id): ?>
                                <li><?php echo e(game()->trans('id')); ?>: <?php echo e($user->game_id); ?></li>
                            <?php endif; ?>
                            <li><?php echo e(trans('messages.profile.info.2fa', ['2fa' => trans_bool($user->hasTwoFactorAuth())])); ?></li>
                        </ul>

                        <?php if($user->hasTwoFactorAuth()): ?>
                            <form action="<?php echo e(route('profile.2fa.disable')); ?>" method="POST">
                                <?php echo csrf_field(); ?>

                                <button type="submit" class="btn btn-danger">
                                    <?php echo e(trans('messages.profile.2fa.disable')); ?>

                                </button>
                            </form>
                        <?php else: ?>
                            <a class="btn btn-primary" href="<?php echo e(route('profile.2fa.index')); ?>"><?php echo e(trans('messages.profile.2fa.enable')); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if(! oauth_login()): ?>
            <?php if(! $user->hasVerifiedEmail()): ?>
                <?php if(session('resent')): ?>
                    <div class="alert alert-success mb-4" role="alert">
                        <?php echo e(trans('auth.verify-sent')); ?>

                    </div>
                <?php endif; ?>

                <div class="alert alert-warning mb-4" role="alert">
                    <p><?php echo e(trans('messages.profile.not-verified')); ?></p>
                    <p><?php echo e(trans('auth.verify-request')); ?></p>

                    <form method="POST" action="<?php echo e(route('verification.resend')); ?>">
                        <?php echo csrf_field(); ?>
                        <button type="submit" class="btn btn-primary">
                            <?php echo e(trans('auth.verify-resend')); ?>

                        </button>
                    </form>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="card shadow-sm mb-4">
                        <div class="card-header"><?php echo e(trans('messages.profile.change-password')); ?></div>
                        <div class="card-body">
                            <form action="<?php echo e(route('profile.password')); ?>" method="POST">
                                <?php echo csrf_field(); ?>

                                <div class="form-group">
                                    <label for="passwordConfirmPassInput"><?php echo e(trans('auth.current-password')); ?></label>
                                    <input type="password" class="form-control <?php $__errorArgs = ['password_confirm_pass'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="passwordConfirmPassInput" name="password_confirm_pass" required>

                                    <?php $__errorArgs = ['password_confirm_pass'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>

                                <div class="form-group">
                                    <label for="passwordInput"><?php echo e(trans('auth.password')); ?></label>
                                    <input type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="passwordInput" name="password" required>

                                    <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>

                                <div class="form-group">
                                    <label for="confirmPasswordInput"><?php echo e(trans('auth.confirm-password')); ?></label>
                                    <input type="password" class="form-control" id="confirmPasswordInput" name="password_confirmation" required>
                                </div>

                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(trans('messages.actions.update')); ?>

                                </button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card shadow-sm mb-4">
                        <div class="card-header"><?php echo e(trans('messages.profile.change-email')); ?></div>
                        <div class="card-body">
                            <form action="<?php echo e(route('profile.email')); ?>" method="POST">
                                <?php echo csrf_field(); ?>

                                <div class="form-group">
                                    <label for="emailInput"><?php echo e(trans('auth.email')); ?></label>
                                    <input type="email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="emailInput" name="email" value="<?php echo e(old('email', $user->email)); ?>" required>

                                    <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>

                                <div class="form-group">
                                    <label for="emailConfirmPassInput"><?php echo e(trans('auth.current-password')); ?></label>
                                    <input type="password" class="form-control <?php $__errorArgs = ['email_confirm_pass'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="emailConfirmPassInput" name="email_confirm_pass" required>

                                    <?php $__errorArgs = ['email_confirm_pass'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>

                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(trans('messages.actions.update')); ?>

                                </button>
                            </form>
                        </div>
                    </div>
                </div>

                <?php if(setting('user_money_transfer')): ?>
                    <div class="col-md-6">
                        <div class="card shadow-sm mb-4">
                            <div class="card-header"><?php echo e(trans('messages.profile.money-transfer.title')); ?></div>
                            <div class="card-body">
                                <form action="<?php echo e(route('profile.transfer-money')); ?>" method="POST">
                                    <?php echo csrf_field(); ?>

                                    <div class="form-group">
                                        <label for="nameInput"><?php echo e(trans('auth.name')); ?></label>
                                        <input type="text" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="nameInput" name="name" value="<?php echo e(old('name')); ?>" required>

                                        <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="moneyInput"><?php echo e(trans('messages.fields.money')); ?></label>
                                        <input type="number" placeholder="0.00" min="0" step="0.01" class="form-control <?php $__errorArgs = ['money'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="moneyInput" name="money" value="<?php echo e(old('money')); ?>" required>

                                        <?php $__errorArgs = ['money'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>

                                    <button type="submit" class="btn btn-primary">
                                        <?php echo e(trans('messages.actions.send')); ?>

                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/profile/index.blade.php ENDPATH**/ ?>