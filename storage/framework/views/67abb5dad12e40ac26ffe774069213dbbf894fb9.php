

<?php $__env->startSection('title', trans('admin.settings.performances.title')); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <h4 class="card-title"><?php echo e(trans('admin.settings.performances.cache.title')); ?></h4>
                    <p class="card-subtitle"><?php echo e(trans('admin.settings.performances.cache.description')); ?></p>

                    <form class="mt-3" action="<?php echo e(route('admin.settings.cache.clear')); ?>" method="POST">
                        <?php echo csrf_field(); ?>

                        <button type="submit" class="btn btn-warning">
                            <i class="fas fa-times"></i> <?php echo e(trans('admin.settings.performances.cache.actions.clear')); ?>

                        </button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <h4 class="card-title"><?php echo e(trans('admin.settings.performances.boost.title')); ?> <i class="text-primary fas fa-rocket"></i></h4>
                    <p class="card-subtitle"><?php echo e(trans('admin.settings.performances.boost.description')); ?></p>
                    <small><?php echo e(trans('admin.settings.performances.boost.info')); ?></small>

                    <p class="mb-3"><?php echo app('translator')->get('admin.settings.performances.boost.current.status', ['status' => trans('admin.settings.performances.boost.current.' . ($cacheStatus ? 'enabled' : 'disabled'))]); ?></p>

                    <form class="d-inline-block" action="<?php echo e(route('admin.settings.cache.advanced.enable')); ?>" method="POST">
                        <?php echo csrf_field(); ?>

                        <?php if($cacheStatus): ?>
                            <button class="btn btn-primary">
                                <i class="fas fa-sync"></i> <?php echo e(trans('admin.settings.performances.boost.actions.reload')); ?>

                            </button>
                        <?php else: ?>
                            <button class="btn btn-primary">
                                <i class="fas fa-check"></i> <?php echo e(trans('admin.settings.performances.boost.actions.enable')); ?>

                            </button>
                        <?php endif; ?>
                    </form>

                    <?php if($cacheStatus): ?>
                        <form class="d-inline-block" action="<?php echo e(route('admin.settings.cache.advanced.clear')); ?>" method="POST">
                            <?php echo csrf_field(); ?>

                            <button class="btn btn-warning">
                                <i class="fas fa-times"></i> <?php echo e(trans('admin.settings.performances.boost.actions.disable')); ?>

                            </button>
                        </form>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/admin/settings/performance.blade.php ENDPATH**/ ?>