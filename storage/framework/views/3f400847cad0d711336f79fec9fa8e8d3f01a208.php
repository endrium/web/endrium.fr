

<?php $__env->startSection('title', trans('admin.plugins.title')); ?>

<?php $__env->startSection('content'); ?>
    <div class="card shadow mb-4">
        <div class="card-header">
            <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('admin.plugins.installed')); ?></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><?php echo e(trans('messages.fields.name')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.author')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.version')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.enabled')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.action')); ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $__currentLoopData = $plugins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $path => $plugin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <th scope="row">
                                <?php if(isset($plugin->url)): ?>
                                    <a href="<?php echo e($plugin->url); ?>" target="_blank" rel="noopener noreferrer">
                                        <?php echo e($plugin->name); ?>

                                    </a>
                                <?php else: ?>
                                    <?php echo e($plugin->name); ?>

                                <?php endif; ?>
                            </th>
                            <td><?php echo e(join(', ', $plugin->authors ?? [])); ?></td>
                            <td><?php echo e($plugin->version); ?></td>
                            <td>
                                <span class="badge badge-<?php echo e(plugins()->isEnabled($path) ? 'success' : 'danger'); ?>">
                                    <?php echo e(trans_bool(plugins()->isEnabled($path))); ?>

                                </span>
                            </td>
                            <td>
                                <form method="POST" action="<?php echo e(route('admin.plugins.' . (plugins()->isEnabled($path) ? 'disable' : 'enable'), $path)); ?>" class="d-inline-block">
                                    <?php echo csrf_field(); ?>

                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fas fa-<?php echo e(plugins()->isEnabled($path)  ? 'times' : 'check'); ?>"></i> <?php echo e(trans('messages.actions.'.(plugins()->isEnabled($path) ? 'disable' : 'enable'))); ?>

                                    </button>
                                </form>
                                <?php if(! plugins()->isEnabled($path)): ?>
                                    <a href="<?php echo e(route('admin.plugins.delete', $path)); ?>" class="btn btn-danger btn-sm" data-confirm="delete">
                                        <i class="fas fa-trash"></i> <?php echo e(trans('messages.actions.delete')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if($pluginsUpdates->has($path)): ?>
                                    <form method="POST" action="<?php echo e(route('admin.plugins.update', $path)); ?>" class="d-inline-block">
                                        <?php echo csrf_field(); ?>

                                        <button type="submit" class="btn btn-info btn-sm">
                                            <i class="fas fa-download"></i> <?php echo e(trans('messages.actions.update')); ?>

                                        </button>
                                    </form>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <?php if(! $availablePlugins->isEmpty()): ?>
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('admin.plugins.available')); ?></h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><?php echo e(trans('messages.fields.name')); ?></th>
                            <th scope="col"><?php echo e(trans('messages.fields.author')); ?></th>
                            <th scope="col"><?php echo e(trans('messages.fields.version')); ?></th>
                            <th scope="col"><?php echo e(trans('messages.fields.action')); ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $__currentLoopData = $availablePlugins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plugin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <th scope="row">
                                    <a href="<?php echo e($plugin['info_url']); ?>" target="_blank" rel="noopener noreferrer">
                                        <?php echo e($plugin['name']); ?>

                                    </a>
                                </th>
                                <td><?php echo e($plugin['author']['name']); ?></td>
                                <td><?php echo e($plugin['version']); ?></td>
                                <td>
                                    <?php if($plugin['premium'] && ! $plugin['purchased']): ?>
                                        <a href="<?php echo e($plugin['info_url']); ?>" class="btn btn-info btn-sm" target="_blank" rel="noopener noreferrer">
                                            <i class="fas fa-shopping-cart"></i> <?php echo e(trans('admin.extensions.buy', ['price' =>  $plugin['price']])); ?>

                                        </a>
                                    <?php else: ?>
                                        <form method="POST" action="<?php echo e(route('admin.plugins.download', $plugin['id'])); ?>">
                                            <?php echo csrf_field(); ?>

                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <i class="fas fa-download"></i> <?php echo e(trans('messages.actions.download')); ?>

                                            </button>
                                        </form>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>

                    <form method="POST" action="<?php echo e(route('admin.plugins.reload')); ?>">
                        <?php echo csrf_field(); ?>

                        <button type="submit" class="btn btn-warning">
                            <i class="fas fa-sync"></i> <?php echo e(trans('messages.actions.reload')); ?>

                        </button>
                    </form>

                </div>
            </div>
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/admin/plugins/index.blade.php ENDPATH**/ ?>