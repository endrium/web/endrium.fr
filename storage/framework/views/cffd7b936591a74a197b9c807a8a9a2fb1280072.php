

<?php $__env->startSection('title', $post->title); ?>
<?php $__env->startSection('description', $post->description); ?>
<?php $__env->startSection('type', 'article'); ?>

<?php $__env->startPush('meta'); ?>
    <meta property="og:article:author:username" content="<?php echo e($post->author->name); ?>">
    <meta property="og:article:published_time" content="<?php echo e($post->published_at->toIso8601String()); ?>">
    <meta property="og:article:modified_time" content="<?php echo e($post->updated_at->toIso8601String()); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content post">
        <?php if(!$post->isPublished()): ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <?php echo e(trans('messages.posts.not-published')); ?>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>

        <div class="card shadow-sm mb-4">
            <div class="card-body">
                <h1 class="card-title"><?php echo e($post->title); ?></h1>

                <?php if($post->hasImage()): ?>
                    <img class="img-fluid rounded mx-auto mb-2" src="<?php echo e($post->imageUrl()); ?>" alt="<?php echo e($post->title); ?>">
                <?php endif; ?>

                <div class="card-text user-html-content">
                    <?php echo $post->content; ?>

                </div>

                <hr>

                <div class="d-flex justify-content-between align-items-center">
                    <button type="button" class="btn btn-primary <?php if($post->isLiked()): ?> active <?php endif; ?>" <?php if(auth()->guard()->guest()): ?> disabled <?php endif; ?> data-like-url="<?php echo e(route('posts.like', $post)); ?>">
                        <?php echo app('translator')->get('messages.likes', ['count' => '<span class="likes-count">'.$post->likes->count().'</span>']); ?>
                        <span class="d-none spinner-border spinner-border-sm load-spinner" role="status"></span>
                    </button>

                    <span><?php echo e(trans('messages.posts.posted', ['date' => format_date($post->published_at), 'user' => $post->author->name])); ?></span>
                </div>
            </div>
        </div>

        <?php $__currentLoopData = $post->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card shadow-sm mb-3">
                <div class="card-header">
                    <?php echo app('translator')->get('messages.comments.author', ['user' => $comment->author->name, 'date' => format_date($comment->created_at, true)]); ?>
                </div>
                <div class="card-body media">
                    <img class="d-flex mr-3 rounded" src="<?php echo e($comment->author->getAvatar()); ?>" alt="<?php echo e($comment->author->name); ?>" height="55">
                    <div class="media-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="content-body">
                                <?php echo e($comment->parseContent()); ?>

                            </div>

                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $comment)): ?>
                                <a class="btn btn-danger" href="<?php echo e(route('posts.comments.destroy', [$post, $comment])); ?>" data-confirm="delete"><?php echo e(trans('messages.actions.delete')); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', \Azuriom\Models\Comment::class)): ?>
            <div class="card mt-4 shadow-sm">
                <div class="card-header">
                    <span class="h5"><?php echo e(trans('messages.comments.create')); ?></span>
                </div>
                <div class="card-body">
                    <form action="<?php echo e(route('posts.comments.store', $post)); ?>" method="POST">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label for="content"><?php echo e(trans('messages.comments.your-comment')); ?></label>
                            <textarea class="form-control <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="content" name="content" rows="4" required></textarea>

                            <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>

                        <button type="submit" class="btn btn-primary"><?php echo e(trans('messages.actions.comment')); ?></button>
                    </form>
                </div>
            </div>
        <?php endif; ?>

        <?php if(auth()->guard()->guest()): ?>
            <div class="alert alert-info" role="alert">
                <?php echo e(trans('messages.comments.guest')); ?>

            </div>
        <?php endif; ?>
    </div>

    <!-- Delete confirm modal -->
    <div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="confirmDeleteLabel"><?php echo e(trans('messages.comments.delete-title')); ?></h2>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body"><?php echo e(trans('messages.comments.delete-description')); ?></div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><?php echo e(trans('messages.actions.cancel')); ?></button>

                    <form id="confirmDeleteForm" method="POST">
                        <?php echo method_field('DELETE'); ?>
                        <?php echo csrf_field(); ?>

                        <button class="btn btn-danger" type="submit"><?php echo e(trans('messages.actions.delete')); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/posts/show.blade.php ENDPATH**/ ?>