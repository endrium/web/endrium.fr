<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h4><?php echo e(trans('theme::prism.footer.about')); ?></h4>

            <p style="font-size: 14px;"><?php echo theme_config('footer_description'); ?></p>
        </div>

        <div class="col-md-4 social">
<!--             <h4><?php echo e(trans('theme::prism.footer.social')); ?></h4> -->
            <h4>Rejoins la communauté</h4>
            <p style="font-size: 12px;">Sur notre discord tu pourra retrouver tous les joueurs d'Endrium.
            Il y a divers salons mis en place afin que votre aventure se déroule dans les meilleures conditions possible, vous pouvez y partager même votre pub lors d'un live ou d'une vidéo sur notre serveur !</p>

<!--             <ul class="list-inline">
                <?php $__currentLoopData = ['twitter', 'youtube', 'discord', 'steam', 'teamspeak', 'instagram']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $social): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($socialLink = theme_config("footer_social_{$social}")): ?>
                        <li class="list-inline-item">
                            <a href="<?php echo e($socialLink); ?>" target="_blank" rel="noreferrer noopener" title="<?php echo e(trans('theme::prism.links.'.$social)); ?>"><i style="font-size: 2.2em;" class="fab fa-<?php echo e($social); ?> fa-2x"></i></a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul> -->

            <a href="https://discord.gg/rvDJbqm" target="_blank" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-discord fa-2x"></i> Discord</a>

        </div>

        <div class="col-md-4 links">
            <!-- <h4><?php echo e(trans('theme::prism.footer.links')); ?></h4> -->
            <h4>Le launcher</h4>

            <a href="https://alexm.best/launcher/releases/win/launcher-endrium-2.2.0.exe" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-windows fa-2x"></i> Launcher Windows</a>
            <a href="https://alexm.best/launcher/releases/linux/launcher-endrium-2.2.0-x86_64.AppImage" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-linux fa-2x"></i> Launcher Linux</a>
            <a href="https://discord.gg/rvDJbqm" target="_blank" class="btn-discord-footer"><i style="font-size: 16px;" class="fab fa-apple fa-2x"></i> Launcher Mac</a>

<!--             <ul class="list-unstyled">
                <?php $__currentLoopData = theme_config('footer_links') ?? []; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                        <a href="<?php echo e($link['value']); ?>"><i class="fas fa-chevron-right"></i> <?php echo e($link['name']); ?></a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul> -->
        </div>

    </div>

    <hr>

    <div class="row footer-bottom">
        <div class="col-md-6">
            <?php echo e(setting('copyright')); ?>

        </div>
        <div class="col-md-6 text-right">
            <?php echo app('translator')->get('messages.copyright'); ?> • 
            <a href="http://www.serveurs-minecraft.org/">Serveur Minecraft org</a>  <a href="http://www.serveur-minecraft.eu/">Serveur Minecraft eu</a>
        </div>
    </div>
</div>
<?php /**PATH /home/endrius/www/resources/themes/prism/views/elements/footer.blade.php ENDPATH**/ ?>