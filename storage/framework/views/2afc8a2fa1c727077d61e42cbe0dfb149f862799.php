

<?php $__env->startSection('title', trans('admin.update.title')); ?>

<?php $__env->startPush('footer-scripts'); ?>
    <script>
        document.querySelectorAll('[data-update-route]').forEach(function (el) {
            el.addEventListener('click', function () {
                const saveButtonIcon = el.querySelector('.btn-spinner');

                el.setAttribute('disabled', '');
                saveButtonIcon.classList.remove('d-none');

                axios.post(el.dataset['updateRoute'])
                    .then(function () {
                        window.location.reload();
                    })
                    .catch(function (error) {
                        createAlert('danger', error.response.data.message ? error.response.data.message : error, true)
                    })
                    .finally(function () {
                        el.removeAttribute('disabled');
                        saveButtonIcon.classList.add('d-none');
                    });
            });
        });

    </script>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
    <div class="card shadow mb-4">
        <div class="card-body">

            <?php if($hasUpdate): ?>
                <h2><?php echo e(trans('admin.update.subtitle-update')); ?></h2>

                <div class="alert alert-warning mt-3" role="alert">
                    <i class="fas fa-exclamation-triangle"></i>
                    <?php echo e(trans('admin.update.backup-info')); ?>

                </div>

                <p><?php echo app('translator')->get('admin.update.update', ['last-version' => $lastVersion, 'version' => Azuriom::version()]); ?></p>

                <?php if($isDownloaded): ?>
                    <p><?php echo e(trans('admin.update.install')); ?></p>

                    <button type="button" class="btn btn-success" data-update-route="<?php echo e(route('admin.update.install')); ?>">
                        <i class="fas fa-download"></i>
                        <?php echo e(trans('admin.update.actions.install')); ?>

                        <span class="spinner-border spinner-border-sm btn-spinner d-none" role="status"></span>
                    </button>
                <?php else: ?>
                    <p><?php echo e(trans('admin.update.download')); ?></p>

                    <button type="button" class="btn btn-primary" data-update-route="<?php echo e(route('admin.update.download')); ?>">
                        <i class="fas fa-cloud-download-alt"></i>
                        <?php echo e(trans('admin.update.actions.download')); ?>

                        <span class="spinner-border spinner-border-sm btn-spinner d-none" role="status"></span>
                    </button>
                <?php endif; ?>

            <?php else: ?>
                <h2><?php echo e(trans('admin.update.subtitle-no-update')); ?></h2>

                <p><?php echo app('translator')->get('admin.update.up-to-date', ['version' => Azuriom::version()]); ?></p>

                <form method="POST" action="<?php echo e(route('admin.update.fetch')); ?>">
                    <?php echo csrf_field(); ?>

                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-sync"></i> <?php echo e(trans('admin.update.actions.check')); ?>

                    </button>
                </form>
            <?php endif; ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/admin/update/index.blade.php ENDPATH**/ ?>