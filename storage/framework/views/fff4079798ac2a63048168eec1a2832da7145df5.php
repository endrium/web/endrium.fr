<span class="dropdown mr-3">
    <a class="dropdown-toggle" href="#" id="notificationsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <!-- Counter - Notifications -->
        <i class="fas fa-bell fa-fw"></i>
        <?php if(! $notifications->isEmpty()): ?>
            <span class="badge badge-danger" id="notificationsCounter"><?php echo e($notifications->count()); ?></span>
        <?php endif; ?>
    </a>

    <!-- Dropdown - Notifications -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right" aria-labelledby="notificationsDropdown">
        <h6 class="dropdown-header"><?php echo e(trans('messages.notifications.notifications')); ?></h6>

        <?php if(! $notifications->isEmpty()): ?>
            <div id="notifications">
                <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="#" class="dropdown-item media align-items-center">
                        <div class="mr-3">
                            <div class="rounded-circle text-white p-1 bg-<?php echo e($notification->level); ?>">
                                <i class="fas fa-<?php echo e($notification->icon()); ?> fa-fw m-2"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="small"><?php echo e(format_date($notification->created_at, true)); ?></div>
                            <?php echo e($notification->content); ?>

                        </div>
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <a href="<?php echo e(route('notifications.read.all')); ?>" id="readNotifications" class="dropdown-item text-center small text-gray-500">
                    <span class="d-none spinner-border spinner-border-sm load-spinner" role="status"></span>
                    <?php echo e(trans('messages.notifications.read')); ?>

                </a>
            </div>
        <?php endif; ?>

        <div id="noNotificationsLabel" class="dropdown-item text-center bg-white small text-success <?php if(! $notifications->isEmpty()): ?> d-none <?php endif; ?>">
            <i class="fas fa-check"></i> <?php echo e(trans('messages.notifications.empty')); ?>

        </div>
    </div>
</span>
<?php /**PATH /home/endrius/www/resources/themes/prism/views/elements/notifications.blade.php ENDPATH**/ ?>