

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><?php echo e(trans('errors.error')); ?></div>

                    <div class="card-body text-center">
                        <h1><?php echo $__env->yieldContent('code'); ?></h1>
                        <h2><?php echo $__env->yieldContent('title'); ?></h2>
                        <p><?php echo $__env->yieldContent('message'); ?></p>

                        <a href="<?php echo e(route('home')); ?>" class="btn btn-primary"><?php echo e(trans('errors.home')); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/errors/layout.blade.php ENDPATH**/ ?>