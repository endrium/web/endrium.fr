<?php
    $colors = [
        'red' => '#c0392b',
        'blue' => '#007bff',
        'green' => '#00db12',
        'purple' => '#9500ff',
        'orange' => '#ffa600',
        'yellow' => '#fff700',
        'aqua' => '#00fbff',
        'pink' => '#ff00cc',
    ];
?>
<!DOCTYPE html>
<?php echo $__env->make('elements.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $__env->yieldContent('description', setting('description', '')); ?>">
    <meta name="theme-color" content="<?php echo e($colors[theme_config('color')] ?? '#3490dc'); ?>">
    <meta name="author" content="Azuriom">

    <meta property="og:title" content="<?php echo $__env->yieldContent('title'); ?>">
    <meta property="og:type" content="<?php echo $__env->yieldContent('type', 'website'); ?>">
    <meta property="og:url" content="<?php echo e(url()->current()); ?>">
    <meta property="og:image" content="<?php echo e(favicon()); ?>">
    <meta property="og:description" content="<?php echo $__env->yieldContent('description', setting('description', '')); ?>">
    <meta property="og:site_name" content="<?php echo e(site_name()); ?>">
    <?php echo $__env->yieldPushContent('meta'); ?>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo $__env->yieldContent('title'); ?> | <?php echo e(site_name()); ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo e(favicon()); ?>">

    <!-- Scripts -->
    <script src="<?php echo e(asset('vendor/jquery/jquery.min.js')); ?>" defer></script>
    <script src="<?php echo e(asset('vendor/bootstrap/js/bootstrap.bundle.min.js')); ?>" defer></script>
    <script src="<?php echo e(asset('vendor/axios/axios.min.js')); ?>" defer></script>
    <script src="<?php echo e(asset('js/script.js')); ?>" defer></script>
    <script src="<?php echo e(theme_asset('js/clipboard.js')); ?>" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/vanilla-tilt/dist/vanilla-tilt.min.js" defer></script>

    <!-- Page level scripts -->
    <?php echo $__env->yieldPushContent('scripts'); ?>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700|Changa+One&display=swap" rel="stylesheet">
    <link href="<?php echo e(asset('vendor/fontawesome/css/all.min.css')); ?>" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(theme_asset('css/style.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(theme_asset('css/colors/'.theme_config('color').'.css')); ?>" rel="stylesheet">
    <?php echo $__env->yieldPushContent('styles'); ?>
</head>

<body data-prism-color="red">
<div id="app">
    <header>
        <?php echo $__env->make('elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </header>

    <main>
        <div class="container">
            <?php echo $__env->make('elements.session-alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <?php echo $__env->yieldContent('content'); ?>
    </main>
</div>

<footer>
    <?php echo $__env->make('elements.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</footer>

<?php echo $__env->yieldPushContent('footer-scripts'); ?>

</body>
</html>
<?php /**PATH /home/endrius/www/resources/themes/prism/views/layouts/app.blade.php ENDPATH**/ ?>