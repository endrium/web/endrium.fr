<?php $__env->startSection('title', trans('faq::messages.title')); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <h1><?php echo e(trans('faq::messages.title')); ?></h1>

        <?php if(empty($questions)): ?>
            <div class="alert alert-info" role="alert">
                <?php echo e(trans('faq::messages.empty')); ?>

            </div>
        <?php else: ?>
            <div class="accordion" id="faq">
                <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card">
                        <div class="card-header px-3 py-4" id="heading<?php echo e($question->id); ?>">
                            <a class="collapsed" data-toggle="collapse" href="#collapse<?php echo e($question->id); ?>" data-target="#collapse<?php echo e($question->id); ?>" aria-expanded="false" aria-controls="collapse<?php echo e($question->id); ?>">
                                <?php echo e($question->name); ?>

                            </a>
                        </div>

                        <div id="collapse<?php echo e($question->id); ?>" class="collapse" aria-labelledby="heading<?php echo e($question->id); ?>" data-parent="#faq">
                            <div class="card-body">
                                <?php echo e($question->parseAnswer()); ?>

                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/faq/resources/views/index.blade.php ENDPATH**/ ?>