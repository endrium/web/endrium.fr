<?php $__env->startSection('title', $discussion->title); ?>

<?php $__env->startPush('meta'); ?>
    <meta property="og:article:author:username" content="<?php echo e($discussion->author->name); ?>">
    <meta property="og:article:published_time" content="<?php echo e($discussion->created_at->toIso8601String()); ?>">
    <meta property="og:article:modified_time" content="<?php echo e($discussion->updated_at->toIso8601String()); ?>">
<?php $__env->stopPush(); ?>

<?php echo $__env->make('forum::elements.markdown-editor', ['editorMinHeight' => 150], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <?php echo $__env->make('forum::elements.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="row mb-2">
            <div class="col-md-9">
                <h1 style="text-transform: uppercase;font-size: 20px;margin: 30px 0px;"><?php echo e($discussion->title); ?></h1>
            </div>

            <div class="col-md-3 d-flex align-items-center justify-content-md-end">
                <div>
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('forum.discussions')): ?>
                        <form action="<?php echo e(route('forum.discussions.'.($discussion->is_pinned ? 'unpin' : 'pin'), $discussion)); ?>" method="POST" class="d-inline-block">
                            <?php echo csrf_field(); ?>

                            <button class="btn btn-success btn-sm <?php if($discussion->is_pinned): ?> active <?php endif; ?>" title="<?php echo e(trans('forum::messages.actions.'.($discussion->is_pinned ? 'unpin' : 'pin'))); ?>" data-toggle="tooltip">
                                <i class="fas fa-thumbtack fa-fw"></i>
                            </button>
                        </form>

                        <form action="<?php echo e(route('forum.discussions.'.($discussion->is_locked ? 'unlock' : 'lock'), $discussion)); ?>" method="POST" class="d-inline-block">
                            <?php echo csrf_field(); ?>

                            <button class="btn btn-secondary btn-sm <?php if($discussion->is_locked): ?> active <?php endif; ?>" title="<?php echo e(trans('forum::messages.actions.'.($discussion->is_locked ? 'unlock' : 'lock'))); ?>" data-toggle="tooltip">
                                <i class="fas fa-lock<?php echo e($discussion->is_locked ? '-open' : ''); ?> fa-fw"></i>
                            </button>
                        </form>
                    <?php endif; ?>

                    <?php if(! $discussion->is_locked || optional(auth()->user())->isAdmin()): ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('update', $discussion)): ?>
                            <a href="<?php echo e(route('forum.discussions.edit', $discussion)); ?>" class="btn btn-info btn-sm" title="<?php echo e(trans('messages.actions.edit')); ?>" data-toggle="tooltip">
                                <i class="fas fa-edit fa-fw"></i>
                            </a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $discussion)): ?>
                            <form action="<?php echo e(route('forum.discussions.destroy', $discussion)); ?>" method="POST" class="d-inline-block" onsubmit="return confirm('<?php echo e(trans('forum::messages.discussions.delete')); ?>')">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>

                                <button type="submit" class="btn btn-danger btn-sm" title="<?php echo e(trans('messages.actions.delete')); ?>" data-toggle="tooltip">
                                    <i class="fas fa-trash fa-fw"></i>
                                </button>
                            </form>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php $__currentLoopData = $discussion->posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-2 col-md-3 text-center">
                            <div class="row">
                                <div class="col-3 col-md-12">
                                    <img src="<?php echo e(game()->getAvatarUrl($post->author, 128)); ?>" alt="<?php echo e($post->author->name); ?>" class="img-fluid mb-3 rounded-lg" style="max-width: 80px">
                                </div>

                                <div class="col-9 col-md-12">
                                    <h5 style="margin-bottom: 10px;"><?php echo e($post->author->name); ?></h5>

                                    <span class="badge badge-label badge-profil" style="<?php echo e($post->author->role->getBadgeStyle()); ?>;"><?php echo e($post->author->role->name); ?></span>
                                </div>
                            </div>

                            <hr>

                            <div class="d-none d-md-block">
                                <ul class="list-unstyled">
                                    <li>
                                        <p><?php echo e(trans('forum::messages.profile.discussions')); ?>: <span style="color: #885ffb;font-weight: 600;"><?php echo e($post->author->discussions_count); ?></span><p>
                                    </li>
                                    <li>
                                        <p><?php echo e(trans('forum::messages.profile.posts')); ?>: <span style="color: #885ffb;font-weight: 600;"><?php echo e($post->author->posts_count); ?></span><p>
                                    </li>
                                    <li>
                                        <p><?php echo e(trans('forum::messages.profile.likes')); ?>: <span style="color: #885ffb;font-weight: 600;"><?php echo e($post->author->likes_count); ?></span><p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-xl-10 col-md-9">
                            <div class="mb-3">
                                <small><?php echo e(format_date($post->created_at, true)); ?></small>
                            </div>

                            <div class="markdown-body mb-3">
                                <?php echo e($post->parseContent()); ?>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-right">
                    <button type="button" class="btn btn-primary btn-sm <?php if($post->isLiked()): ?> active <?php endif; ?>" <?php if(auth()->guard()->guest()): ?> disabled <?php endif; ?> data-like-url="<?php echo e(route('forum.posts.like', $post)); ?>">
                        <i class="fas fa-thumbs-up"></i>
                        <span class="likes-count"><?php echo e($post->likes->count()); ?></span>
                        <span class="d-none spinner-border spinner-border-sm load-spinner" role="status"></span>
                    </button>

                    <?php if(! $loop->first && (! $discussion->is_locked || optional(auth()->user())->isAdmin())): ?>
                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit', $post)): ?>
                            <a href="<?php echo e(route('forum.discussions.posts.edit', [$discussion, $post])); ?>" class="btn btn-info btn-sm" title="<?php echo e(trans('messages.actions.edit')); ?>" data-toggle="tooltip">
                                <i class="fas fa-edit fa-fw"></i>
                            </a>
                        <?php endif; ?>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete', $post)): ?>
                            <form action="<?php echo e(route('forum.discussions.posts.destroy', [$discussion, $post])); ?>" method="POST" class="d-inline-block" onsubmit="return confirm('<?php echo e(trans('forum::messages.posts.delete')); ?>')">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>

                                <button type="submit" class="btn btn-danger btn-sm" title="<?php echo e(trans('messages.actions.delete')); ?>" data-toggle="tooltip">
                                    <i class="fas fa-trash fa-fw"></i>
                                </button>
                            </form>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php echo e($discussion->posts->links()); ?>


        <?php if($discussion->is_locked): ?>
            <div class="alert alert-warning" role="alert">
                <i class="fas fa-lock"></i> <?php echo e(trans('forum::messages.discussions.info-locked')); ?>

            </div>
        <?php endif; ?>

        <?php if(! $discussion->is_locked || optional(auth()->user())->isAdmin()): ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', \Azuriom\Plugin\Forum\Models\Post::class)): ?>
                <div class="card shadow-sm mb-3">
                    <div class="card-header">
                        <span class="h5"><?php echo e(trans('forum::messages.discussions.respond')); ?></span>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo e(route('forum.discussions.posts.store', $discussion)); ?>" method="POST">
                            <?php echo csrf_field(); ?>

                            <div class="form-group">
                                <label for="content"><?php echo e(trans('messages.fields.content')); ?></label>
                                <textarea class="form-control <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="content" name="content" rows="4"></textarea>

                                <?php $__errorArgs = ['content'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>

                            <button type="submit" class="btn btn-primary">
                                <i class="fas fa-reply"></i> <?php echo e(trans('forum::messages.discussions.respond')); ?>

                            </button>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/forum/resources/views/discussions/show.blade.php ENDPATH**/ ?>