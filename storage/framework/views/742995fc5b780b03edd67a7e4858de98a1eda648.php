

<?php $__env->startSection('title', trans('messages.home')); ?>

<?php $__env->startSection('content'); ?>
    <div class="home-background <?php if(theme_config('title')): ?> background-overlay <?php endif; ?> mb-4" style="background: url('<?php echo e(setting('background') ? image_url(setting('background')) : 'https://via.placeholder.com/2000x500'); ?>') no-repeat center / cover">
        <?php if(theme_config('title')): ?>
            <div class="container h-100">
                <div class="row align-items-center justify-content-center h-100">

                    <div class="col-md-6 text-center" style="margin-top: -8%;">
                        <p class="text-server">Bienvenue sur le site</p>
                        <h1 class="title-server">Endrium</h1>
                    </div>

                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="container">
        <div class="content-box">

            <div class="row">
                <div class="col-md-8">
                    <div class="box-page">
                
                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="col-md-12">
                                <div class="row" style="padding: 12px;">
                                    <div class="col-md-5">
                                        <a href="<?php echo e(route('posts.show', $post->slug)); ?>" class="link-unstyled">
                                            <?php if($post->hasImage()): ?>
                                            <img src="<?php echo e($post->imageUrl()); ?>" alt="<?php echo e($post->title); ?>" class="img-fluid rounded">
                                        </a>
                                    </div>
                                    <div class="col-md-7">
                                        <a href="<?php echo e(route('posts.show', $post->slug)); ?>" class="link-unstyled">
                                            <div class="title-news p-2"><?php echo e($post->title); ?></div>
                                        </a>
                                        <p class="text-news p-2"><?php echo e(Str::limit(strip_tags($post->content), 240)); ?></p>
                                        <?php else: ?>
                                            <div class="preview-content p-4">
                                                <h4><?php echo e($post->title); ?></h4>
                                                <?php echo e(Str::limit(strip_tags($post->content), 240)); ?>

                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        

                    </div>

                    <div class="alert alert-warning" role="alert">
                      D'autres nouveautés prochainement ...
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="box-page">
                        <h4 class="title-box">Rejoignez-nous sur discord</h4>
                    </div>
                    <div class="box-page">
                        <iframe src="https://discordapp.com/widget?id=706922285944012852&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" 
                        style="margin: auto;display:block;"></iframe>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/themes/prism/views/home.blade.php ENDPATH**/ ?>