<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>"><?php echo e(trans('messages.home')); ?></a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('forum.home')); ?>"><?php echo e(trans('forum::messages.title')); ?></a></li>

        <?php $__currentLoopData = optional($current ?? null)->getNavigationStack() ?? []; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumbLink => $breadcrumbName): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="breadcrumb-item"><a href="<?php echo e($breadcrumbLink); ?>"><?php echo e($breadcrumbName); ?></a></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ol>
</nav><?php /**PATH /home/endrius/www/plugins/forum/resources/views/elements/nav.blade.php ENDPATH**/ ?>