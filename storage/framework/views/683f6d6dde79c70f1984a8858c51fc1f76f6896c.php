<?php $__env->startSection('title', trans('support::messages.title')); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">
        <h1><?php echo e(trans('support::messages.title')); ?></h1>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><?php echo e(trans('support::messages.fields.subject')); ?></th>
                <th scope="col"><?php echo e(trans('support::messages.fields.category')); ?></th>
                <th scope="col"><?php echo e(trans('messages.fields.status')); ?></th>
                <th scope="col"><?php echo e(trans('messages.fields.date')); ?></th>
            </tr>
            </thead>
            <tbody>

            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <th scope="row"><?php echo e($ticket->id); ?></th>
                    <td>
                        <a href="<?php echo e(route('support.tickets.show', $ticket)); ?>"><?php echo e($ticket->subject); ?></a>
                    </td>
                    <td><?php echo e($ticket->category->name); ?></td>
                    <td>
                        <span class="badge badge-<?php echo e($ticket->isClosed() ? 'danger' : 'success'); ?>">
                            <?php echo e($ticket->statusMessage()); ?>

                        </span>
                    </td>
                    <td><?php echo e(format_date_compact($ticket->created_at)); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </tbody>
        </table>

        <a href="<?php echo e(route('support.tickets.create')); ?>" class="btn btn-success">
            <?php echo e(trans('support::messages.actions.open-new')); ?>

        </a>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/support/resources/views/tickets/index.blade.php ENDPATH**/ ?>