

<?php $__env->startSection('title', trans('vote::messages.title')); ?>

<?php $__env->startSection('content'); ?>
    <div class="container content">

        <h2><?php echo e(trans('vote::messages.sections.vote')); ?></h2>

        <div id="vote-alert"></div>

        <div class="vote vote-now text-center position-relative mb-4 px-3 py-5 border rounded">
            <div class="<?php if(auth()->guard()->check()): ?> d-none <?php endif; ?>" data-vote-step="1">
                <form id="voteNameForm">
                    <div class="form-group">
                        <input type="text" id="stepNameInput" name="name" class="form-control" <?php if(auth()->guard()->check()): ?> value="<?php echo e(auth()->user()->name); ?>" <?php endif; ?> placeholder="<?php echo e(trans('messages.fields.name')); ?>" required>
                    </div>

                    <button type="submit" class="btn btn-primary">
                        <?php echo e(trans('messages.actions.continue')); ?>

                        <span class="d-none spinner-border spinner-border-sm load-spinner" role="status"></span>
                    </button>
                </form>
            </div>

            <div class="<?php if(auth()->guard()->guest()): ?> d-none <?php endif; ?> h-100" data-vote-step="2">
                <div id="vote-spinner" class="d-none h-100">
                    <div class="spinner-border text-white" role="status"></div>
                </div>

                <?php $__empty_1 = true; $__currentLoopData = $sites; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $site): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <a class="btn btn-primary" href="<?php echo e($site->url); ?>" target="_blank" rel="noopener noreferrer" data-site-url="<?php echo e(route('vote.vote', $site)); ?>"><?php echo e($site->name); ?></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="alert alert-warning" role="alert"><?php echo e(trans('vote::messages.no-site')); ?></div>
                <?php endif; ?>
            </div>
            <div class="d-none" data-vote-step="3">
                <p id="vote-result"></p>
            </div>
        </div>

        <h2><?php echo e(trans('vote::messages.sections.top')); ?></h2>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><?php echo e(trans('messages.fields.name')); ?></th>
                <th scope="col"><?php echo e(trans('vote::messages.fields.votes')); ?></th>
            </tr>
            </thead>
            <tbody>

            <?php $__currentLoopData = $votes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $vote): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <th scope="row">#<?php echo e($id); ?></th>
                    <td><?php echo e($vote['user']->name); ?></td>
                    <td><?php echo e($vote['votes']); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </tbody>
        </table>

        <?php if(display_rewards()): ?>
            <h2><?php echo e(trans('vote::messages.sections.rewards')); ?></h2>

            <table class="table">
                <thead>
                <tr>
                    <th scope="col"><?php echo e(trans('messages.fields.name')); ?></th>
                    <th scope="col"><?php echo e(trans('vote::messages.fields.chances')); ?></th>
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $rewards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reward): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <th scope="row"><?php echo e($reward->name); ?></th>
                        <td><?php echo e($reward->chances); ?> %</td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </tbody>
            </table>
        <?php endif; ?>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <script src="<?php echo e(plugin_asset('vote', 'js/vote.js')); ?>" defer></script>
    <script>
        const voteRoute = '<?php echo e(route('vote.verify-user', '')); ?>';
        let username <?php if(auth()->guard()->check()): ?> = '<?php echo e(auth()->user()->name); ?>' <?php endif; ?>;
    </script>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('styles'); ?>
    <style>
        #vote-spinner {
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: rgba(70, 70, 70, 0.6);
            z-index: 10;
        }
    </style>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/vote/resources/views/index.blade.php ENDPATH**/ ?>