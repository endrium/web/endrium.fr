<?php if($reCaptchaKey = setting('recaptcha-site-key')): ?>
    <?php $__env->startPush('scripts'); ?>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <?php $__env->stopPush(); ?>

    <?php $__env->startPush('footer-scripts'); ?>
        <script>
            const captchaForm = document.getElementById('captcha-form');

            function submitCaptchaForm() {
                captchaForm.submit();
            }

            if (captchaForm) {
                captchaForm.addEventListener('submit', function (e) {
                    e.preventDefault();

                    grecaptcha.execute();
                });
            }
        </script>
    <?php $__env->stopPush(); ?>

    <div class="g-recaptcha" data-sitekey="<?php echo e($reCaptchaKey); ?>" data-callback="submitCaptchaForm" data-size="invisible"></div>
<?php endif; ?>
<?php /**PATH /home/endrius/www/resources/views/elements/captcha.blade.php ENDPATH**/ ?>