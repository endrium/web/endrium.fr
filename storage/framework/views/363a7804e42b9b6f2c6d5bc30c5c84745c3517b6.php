

<?php $__env->startSection('title', trans('admin.themes.title')); ?>

<?php $__env->startSection('content'); ?>
    <div class="card shadow mb-4">
        <div class="card-header">
            <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('admin.themes.current.title')); ?></h6>
        </div>
        <div class="card-body">
            <?php if($current): ?>
                <h3 class="h5"><?php echo e($current->name); ?></h3>
                <ul>
                    <li><?php echo e(trans('admin.themes.current.author', ['author' => join(', ', $current->authors)])); ?></li>
                    <li><?php echo e(trans('admin.themes.current.version', ['version' => $current->version])); ?></li>
                </ul>

                <form action="<?php echo e(route('admin.themes.change')); ?>" method="POST" class="d-inline-block">
                    <?php echo csrf_field(); ?>

                    <?php if($currentHasConfig): ?>
                        <a class="btn btn-primary" href="<?php echo e(route('admin.themes.edit', $currentPath)); ?>">
                            <i class="fas fa-wrench"></i> <?php echo e(trans('admin.themes.actions.edit-config')); ?>

                        </a>
                    <?php endif; ?>

                    <button type="submit" class="btn btn-warning">
                        <i class="fas fa-times"></i> <?php echo e(trans('admin.themes.actions.disable')); ?>

                    </button>
                </form>
                <?php if($themesUpdates->has($currentPath)): ?>
                    <form method="POST" action="<?php echo e(route('admin.themes.update', $currentPath)); ?>" class="d-inline-block">
                        <?php echo csrf_field(); ?>

                        <button type="submit" class="btn btn-info">
                            <i class="fas fa-download"></i> <?php echo e(trans('messages.actions.update')); ?>

                        </button>
                    </form>
                <?php endif; ?>
            <?php else: ?>
                <?php echo e(trans('admin.themes.no-enabled')); ?>

            <?php endif; ?>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header">
            <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('admin.themes.installed')); ?></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><?php echo e(trans('messages.fields.name')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.author')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.version')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.action')); ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $__currentLoopData = $themes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $path => $theme): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <th scope="row">
                                <?php if(isset($theme->url)): ?>
                                    <a href="<?php echo e($theme->url); ?>" target="_blank" rel="noopener noreferrer">
                                        <?php echo e($theme->name); ?>

                                    </a>
                                <?php else: ?>
                                    <?php echo e($theme->name); ?>

                                <?php endif; ?>
                            </th>
                            <td><?php echo e(join(', ', $theme->authors ?? [])); ?></td>
                            <td><?php echo e($theme->version); ?></td>
                            <td>
                                <form method="POST" action="<?php echo e(route('admin.themes.change', $path)); ?>" class="d-inline-block">
                                    <?php echo csrf_field(); ?>

                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fas fa-check"></i> <?php echo e(trans('messages.actions.enable')); ?>

                                    </button>
                                </form>
                                <a href="<?php echo e(route('admin.themes.delete', $path)); ?>" class="btn btn-danger btn-sm" data-confirm="delete">
                                    <i class="fas fa-trash"></i> <?php echo e(trans('messages.actions.delete')); ?>

                                </a>
                                <?php if($themesUpdates->has($path)): ?>
                                    <form method="POST" action="<?php echo e(route('admin.themes.update', $path)); ?>" class="d-inline-block">
                                        <?php echo csrf_field(); ?>

                                        <button type="submit" class="btn btn-info btn-sm">
                                            <i class="fas fa-download"></i> <?php echo e(trans('messages.actions.update')); ?>

                                        </button>
                                    </form>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php if(! $availableThemes->isEmpty()): ?>
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('admin.themes.available')); ?></h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><?php echo e(trans('messages.fields.name')); ?></th>
                            <th scope="col"><?php echo e(trans('messages.fields.author')); ?></th>
                            <th scope="col"><?php echo e(trans('messages.fields.version')); ?></th>
                            <th scope="col"><?php echo e(trans('messages.fields.action')); ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $__currentLoopData = $availableThemes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $theme): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <th scope="row">
                                    <a href="<?php echo e($theme['info_url']); ?>" target="_blank" rel="noopener noreferrer">
                                        <?php echo e($theme['name']); ?>

                                    </a>
                                </th>
                                <td><?php echo e($theme['author']['name']); ?></td>
                                <td><?php echo e($theme['version']); ?></td>
                                <td>
                                    <?php if($theme['premium'] && ! $theme['purchased']): ?>
                                        <a href="<?php echo e($theme['info_url']); ?>" class="btn btn-info btn-sm" target="_blank" rel="noopener noreferrer">
                                            <i class="fas fa-shopping-cart"></i> <?php echo e(trans('admin.extensions.buy', ['price' =>  $theme['price']])); ?>

                                        </a>
                                    <?php else: ?>
                                        <form method="POST" action="<?php echo e(route('admin.themes.download', $theme['id'])); ?>">
                                            <?php echo csrf_field(); ?>

                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <i class="fas fa-download"></i> <?php echo e(trans('messages.actions.download')); ?>

                                            </button>
                                        </form>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>

                    <form method="POST" action="<?php echo e(route('admin.themes.reload')); ?>">
                        <?php echo csrf_field(); ?>

                        <button type="submit" class="btn btn-warning">
                            <i class="fas fa-sync"></i> <?php echo e(trans('messages.actions.reload')); ?>

                        </button>
                    </form>

                </div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/admin/themes/index.blade.php ENDPATH**/ ?>