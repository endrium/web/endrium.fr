<?php $__env->startSection('title', trans('support::admin.title')); ?>

<?php $__env->startSection('content'); ?>

    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('support.categories')): ?>
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('support::admin.settings.title')); ?></h6>
            </div>
            <div class="card-body">
                <form action="<?php echo e(route('support.admin.settings.update')); ?>" method="POST">
                    <?php echo csrf_field(); ?>

                    <div class="form-group">
                        <label for="webhookInput"><?php echo e(trans('support::admin.settings.webhook')); ?></label>
                        <input type="text" class="form-control <?php $__errorArgs = ['webhook'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="webhookInput" name="webhook" placeholder="https://discordapp.com/api/webhooks/.../..." value="<?php echo e(old('webhook', setting('support.webhook'))); ?>" aria-describedby="webhookInfo">

                        <?php $__errorArgs = ['webhook'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert"><strong><?php echo e($message); ?></strong></span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>

                        <small id="webhookInfo" class="form-text"><?php echo e(trans('support::admin.settings.webhook-info')); ?></small>
                    </div>

                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-save"></i> <?php echo e(trans('messages.actions.update')); ?>

                    </button>
                </form>
            </div>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('support::admin.categories.title')); ?></h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col"><?php echo e(trans('messages.fields.name')); ?></th>
                            <th scope="col"><?php echo e(trans('messages.fields.action')); ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <th scope="row"><?php echo e($category->id); ?></th>
                                <td><?php echo e($category->name); ?></td>
                                <td>
                                    <a href="<?php echo e(route('support.admin.categories.edit', $category)); ?>" class="mx-1" title="<?php echo e(trans('messages.actions.edit')); ?>" data-toggle="tooltip"><i class="fas fa-edit"></i></a>
                                    <a href="<?php echo e(route('support.admin.categories.destroy', $category)); ?>" class="mx-1" title="<?php echo e(trans('messages.actions.delete')); ?>" data-toggle="tooltip" data-confirm="delete"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                </div>

                <a class="btn btn-primary" href="<?php echo e(route('support.admin.categories.create')); ?>">
                    <i class="fas fa-plus"></i> <?php echo e(trans('messages.actions.add')); ?>

                </a>
            </div>
        </div>
    <?php endif; ?>

    <div class="card shadow mb-4">
        <div class="card-header">
            <h6 class="m-0 font-weight-bold text-primary"><?php echo e(trans('support::admin.tickets.title')); ?></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col"><?php echo e(trans('support::messages.fields.subject')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.author')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.status')); ?></th>
                        <th scope="col"><?php echo e(trans('support::messages.fields.category')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.date')); ?></th>
                        <th scope="col"><?php echo e(trans('messages.fields.action')); ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <th scope="row"><?php echo e($ticket->id); ?></th>
                            <td><?php echo e($ticket->subject); ?></td>
                            <td><?php echo e($ticket->author->name); ?></td>
                            <td>
                                <span class="badge badge-<?php echo e($ticket->isClosed() ? 'danger' : 'success'); ?>">
                                    <?php echo e($ticket->statusMessage()); ?>

                                </span>
                            </td>
                            <td><?php echo e($ticket->category->name); ?></td>
                            <td><?php echo e(format_date_compact($ticket->created_at)); ?></td>
                            <td>
                                <a href="<?php echo e(route('support.admin.tickets.show', $ticket)); ?>" class="mx-1" title="<?php echo e(trans('messages.actions.show')); ?>" data-toggle="tooltip"><i class="fas fa-eye"></i></a>
                                <?php if($ticket->isClosed()): ?>
                                    <a href="<?php echo e(route('support.admin.tickets.destroy', $ticket)); ?>" class="mx-1" title="<?php echo e(trans('messages.actions.delete')); ?>" data-toggle="tooltip" data-confirm="delete"><i class="fas fa-trash"></i></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>
                </table>
            </div>

            <?php echo e($tickets->links()); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/plugins/support/resources/views/admin/tickets/index.blade.php ENDPATH**/ ?>