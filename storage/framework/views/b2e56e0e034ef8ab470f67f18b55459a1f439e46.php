<?php $__env->startPush('styles'); ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/easymde@2.9.0/dist/easymde.min.css">
<?php $__env->stopPush(); ?>

<?php $__env->startPush('footer-scripts'); ?>
    <script src="https://cdn.jsdelivr.net/npm/easymde@2.9.0/dist/easymde.min.js"></script>
    <script>
        const markdownArea = document.querySelector('textarea');

        if (markdownArea) {
            new EasyMDE({
                element: markdownArea,
                autoDownloadFontAwesome: false,
                minHeight: '<?php echo e($editorMinHeight ?? 300); ?>px',
                promptURLs: true,
                spellChecker: false,
                showIcons: ['strikethrough', 'code', 'horizontal-rule', 'undo', 'redo'],
                status: false,
            });
        }
    </script>
<?php $__env->stopPush(); ?>
<?php /**PATH /home/endrius/www/plugins/forum/resources/views/elements/markdown-editor.blade.php ENDPATH**/ ?>