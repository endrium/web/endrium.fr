<?php if($htmlScriptsHead = setting('html-head')): ?> <?php $__env->startPush('scripts'); ?>
    <?php echo $htmlScriptsHead; ?>

<?php $__env->stopPush(); ?> <?php endif; ?>

<?php if($htmlScriptsBody = setting('html-body')): ?> <?php $__env->startPush('footer-scripts'); ?>
    <?php echo $htmlScriptsBody; ?>

<?php $__env->stopPush(); ?> <?php endif; ?>

<?php if($keywords = setting('keywords')): ?> <?php $__env->startPush('meta'); ?>
    <meta name="keywords" content="<?php echo e($keywords); ?>">
<?php $__env->stopPush(); ?> <?php endif; ?>

<?php if(($welcomePopup = setting('welcome-popup')) && ! session()->has('welcome_popup')): ?>
    <?php $__env->startPush('footer-scripts'); ?>
        <!-- Modal -->
        <div class="modal fade" id="welcomePopupModal" tabindex="-1" role="dialog" aria-labelledby="welcomePopupLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="welcomePopupLabel"><?php echo e(site_name()); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(trans('close')); ?>">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo $welcomePopup; ?>

                    </div>
                </div>
            </div>
        </div>

        <script>
            window.addEventListener('load', function () {
                setTimeout(function () {
                    $('#welcomePopupModal').modal('show');
                }, 500);
            });
        </script>
    <?php $__env->stopPush(); ?>

    <?php
        session()->put('welcome_popup', true);
    ?>
<?php endif; ?>
<?php /**PATH /home/endrius/www/resources/views/elements/base.blade.php ENDPATH**/ ?>