

<?php $__env->startSection('title', trans('errors.404.title')); ?>
<?php $__env->startSection('code', '404'); ?>
<?php $__env->startSection('message', trans('errors.404.message')); ?>

<?php echo $__env->make('errors::layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/endrius/www/resources/views/errors/404.blade.php ENDPATH**/ ?>