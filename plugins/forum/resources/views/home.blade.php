@extends('layouts.app')

@section('title', trans('forum::messages.title'))

@section('content')

    <div class="home-background @if(theme_config('title')) background-overlay @endif mb-4" style="background: url('{{ setting('background') ? image_url(setting('background')) : 'https://via.placeholder.com/2000x500' }}') no-repeat center / cover">
        @if(theme_config('title'))
            <div class="container h-100">
                <div class="row align-items-center justify-content-center h-100">

                    <div class="col-md-6 text-center" style="margin-top: -8%;">
                        <p class="text-server">Echange avec la communauté</p>
                        <h1 class="title-server">Endrium</h1>
                    </div>

                </div>
            </div>
        @endif
    </div>

    <div class="container content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="margin-top: -18%;position: relative;background: #ffffff1f;">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ trans('messages.home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('forum.home') }}">{{ trans('forum::messages.title') }}</a></li>

                @foreach(optional($current ?? null)->getNavigationStack() ?? [] as $breadcrumbLink => $breadcrumbName)
                    <li class="breadcrumb-item"><a href="{{ $breadcrumbLink }}">{{ $breadcrumbName }}</a></li>
                @endforeach
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-9">
                @foreach($categories as $category)
                    <div class="card mb-3">
                        <div class="card-header">
                            <h2 class="h3">{{ $category->name }}</h2>
                            <small>{{ $category->description }}</small>
                        </div>

                        <div class="list-group list-group-flush">
                            @foreach($category->forums as $forum)
                                <div class="list-group-item box-forum">
                                    <div class="row">
                                        <div class="col-xl-1 col-md-2 col-2 text-center">
                                            <i class="{{ $forum->icon ?? 'fas fa-comments' }} fa-2x fa-fw forum-big-icon icon-forum"></i>
                                        </div>

                                        <div class="col-xl-8 col-md-7 col-10 pl-md-0">
                                            <h3 class="h5">
                                                <a href="{{ route('forum.show', $forum->slug) }}" class="titre-forum">{{ $forum->name }}</a>
                                            </h3>

                                           <p> {{ $forum->description ?? '' }}</p>
                                        </div>

                                        <div class="col-xl-3 col-md-3 d-none d-md-block">
                                            <p>{{ trans_choice('forum::messages.forums.discussions-count', $forum->discussions_count) }}</p>
                                            <p>{{ trans_choice('forum::messages.discussions.posts-count', $forum->posts_count) }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-comments fa-fw"></i> {{ trans('forum::messages.latest.title') }}
                    </div>
                    <div class="list-group list-group-flush">
                        @foreach($latestPosts as $post)
                            <div class="list-group-item">
                                <a href="{{ route('forum.discussions.show', $post->discussion) }}">
                                    {{ $post->discussion->title }}
                                </a>

                                <br>

                                <small>{{ $post->author->name }}, {{ format_date($post->created_at) }}</small>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-chart-bar fa-fw"></i> {{ trans('forum::messages.stats.title') }}
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled mb-0">
                            <li>{{ trans_choice('forum::messages.stats.discussions', $discussionsCount) }}</li>
                            <li>{{ trans_choice('forum::messages.stats.posts', $postsCount) }}</li>
                            <li>{{ trans_choice('forum::messages.stats.users', $usersCount) }}</li>
                        </ul>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-users fa-fw"></i> {{ trans('forum::messages.online.title') }}
                    </div>
                    <div class="card-body">
                        @empty($onlineUsers)
                            {{ trans('forum::messages.online.none') }}
                        @else
                            {{ implode(', ', $onlineUsers) }}
                        @endempty
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
