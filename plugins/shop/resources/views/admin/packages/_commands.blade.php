@push('footer-scripts')
    <script>
        function addCommandListener(el) {
            el.addEventListener('click', function () {
                const element = el.parentNode.parentNode;

                element.parentNode.removeChild(element);
            });
        }

        document.querySelectorAll('.command-remove').forEach(function (el) {
            addCommandListener(el);
        });

        document.getElementById('addCommandButton').addEventListener('click', function () {
            let input = '<div class="input-group mb-2"><input type="text" name="commands[]" class="form-control"><div class="input-group-append">';
            input += '<button class="btn btn-outline-danger command-remove" type="button"><i class="fas fa-times"></i></button>';
            input += '</div></div>';

            const newElement = document.createElement('div');
            newElement.innerHTML = input;

            addCommandListener(newElement.querySelector('.command-remove'));

            document.getElementById('commands').appendChild(newElement);
        });
    </script>
@endpush
