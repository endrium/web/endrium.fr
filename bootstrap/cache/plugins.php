<?php return array (
  'advancedban' => 
  array (
    'id' => 'advancedban',
    'name' => 'AdvancedBan',
    'description' => 'Plugin for Azuriom',
    'version' => '1.0.1',
    'url' => 'https://azuriom.com',
    'authors' => 
    array (
      0 => 'ZeProf2Coding',
    ),
    'providers' => 
    array (
      0 => '\\Azuriom\\Plugin\\AdvancedBan\\Providers\\AdvancedBanServiceProvider',
      1 => '\\Azuriom\\Plugin\\AdvancedBan\\Providers\\RouteServiceProvider',
    ),
    'apiId' => 26,
    'composer' => 
    array (
      'name' => 'azuriom/advancedban',
      'type' => 'azuriom-plugin',
      'description' => 'Plugin for Azuriom',
      'autoload' => 
      array (
        'psr-4' => 
        array (
          'Azuriom\\Plugin\\AdvancedBan\\' => 'src/',
        ),
        'files' => 
        array (
          0 => 'src/helpers.php',
        ),
      ),
    ),
  ),
  'cloudflare' => 
  array (
    'id' => 'cloudflare',
    'name' => 'Cloudflare Support',
    'description' => 'Rewrite original visitors IPs and add support for flexible SSL when using Cloudflare.',
    'version' => '1.0.1',
    'url' => 'https://azuriom.com/market/resources/12',
    'authors' => 
    array (
      0 => 'Azuriom',
    ),
    'providers' => 
    array (
      0 => '\\Azuriom\\Plugin\\CloudflareSupport\\Providers\\CloudflareSupportServiceProvider',
    ),
    'apiId' => 12,
    'composer' => 
    array (
      'name' => 'azuriom/cloudflare-support',
      'type' => 'azuriom-plugin',
      'description' => 'Plugin for Azuriom',
      'autoload' => 
      array (
        'psr-4' => 
        array (
          'Azuriom\\Plugin\\CloudflareSupport\\' => 'src/',
        ),
      ),
    ),
  ),
  'faq' => 
  array (
    'id' => 'faq',
    'name' => 'FAQ',
    'description' => 'A FAQ plugin to make a FAQ to answer the most frequently asked questions.',
    'version' => '0.1.2',
    'url' => 'https://azuriom.com/market/resources/4',
    'authors' => 
    array (
      0 => 'Azuriom',
    ),
    'providers' => 
    array (
      0 => '\\Azuriom\\Plugin\\FAQ\\Providers\\FAQServiceProvider',
      1 => '\\Azuriom\\Plugin\\FAQ\\Providers\\RouteServiceProvider',
    ),
    'apiId' => 4,
    'composer' => 
    array (
      'name' => 'azuriom/faq',
      'type' => 'azuriom-plugin',
      'description' => 'FAQ plugin for Azuriom',
      'license' => 'MIT',
      'autoload' => 
      array (
        'psr-4' => 
        array (
          'Azuriom\\Plugin\\FAQ\\' => 'src/',
        ),
        'files' => 
        array (
          0 => 'src/helpers.php',
        ),
      ),
    ),
  ),
  'forum' => 
  array (
    'id' => 'forum',
    'name' => 'Forum',
    'description' => 'A nice and simple forum for Azuriom',
    'version' => '0.1.8',
    'url' => 'https://azuriom.com/market/resources/16',
    'authors' => 
    array (
      0 => 'Azuriom',
    ),
    'providers' => 
    array (
      0 => '\\Azuriom\\Plugin\\Forum\\Providers\\ForumServiceProvider',
      1 => '\\Azuriom\\Plugin\\Forum\\Providers\\RouteServiceProvider',
    ),
    'apiId' => 16,
    'composer' => 
    array (
      'name' => 'azuriom/forum',
      'type' => 'azuriom-plugin',
      'description' => 'A nice and simple forum for Azuriom',
      'autoload' => 
      array (
        'psr-4' => 
        array (
          'Azuriom\\Plugin\\Forum\\' => 'src/',
        ),
        'files' => 
        array (
          0 => 'src/helpers.php',
        ),
      ),
    ),
  ),
  'shop' => 
  array (
    'id' => 'shop',
    'name' => 'Shop',
    'description' => 'A shop plugin to sell in-game items on your website.',
    'version' => '0.1.10',
    'url' => 'https://azuriom.com/market/resources/1',
    'authors' => 
    array (
      0 => 'Azuriom',
    ),
    'providers' => 
    array (
      0 => '\\Azuriom\\Plugin\\Shop\\Providers\\ShopServiceProvider',
      1 => '\\Azuriom\\Plugin\\Shop\\Providers\\RouteServiceProvider',
    ),
    'apiId' => 1,
    'composer' => 
    array (
      'name' => 'azuriom/shop',
      'type' => 'azuriom-plugin',
      'description' => 'Complete shop plugin for your server',
      'license' => 'MIT',
      'autoload' => 
      array (
        'psr-4' => 
        array (
          'Azuriom\\Plugin\\Shop\\' => 'src/',
        ),
        'files' => 
        array (
          0 => 'src/helpers.php',
        ),
      ),
      'require' => 
      array (
        'php' => '^7.2',
        'mollie/mollie-api-php' => '^2.12.0',
        'paymentwall/paymentwall-php' => '^2.2',
        'paygol/php-sdk' => '^1.0',
        'paypal/rest-api-sdk-php' => '^1.14.0',
        'stripe/stripe-php' => '^7.14',
      ),
      'replace' => 
      array (
        'guzzlehttp/guzzle' => '*',
      ),
    ),
  ),
  'support' => 
  array (
    'id' => 'support',
    'name' => 'Support',
    'description' => 'A support plugin that allows users to create a ticket when they needs help.',
    'version' => '0.1.3',
    'url' => 'https://azuriom.com/market/resources/3',
    'authors' => 
    array (
      0 => 'Azuriom',
    ),
    'providers' => 
    array (
      0 => '\\Azuriom\\Plugin\\Support\\Providers\\SupportServiceProvider',
      1 => '\\Azuriom\\Plugin\\Support\\Providers\\RouteServiceProvider',
    ),
    'apiId' => 3,
    'composer' => 
    array (
      'name' => 'azuriom/support',
      'type' => 'azuriom-plugin',
      'description' => 'Support plugin for Azuriom',
      'license' => 'MIT',
      'autoload' => 
      array (
        'psr-4' => 
        array (
          'Azuriom\\Plugin\\Support\\' => 'src/',
        ),
        'files' => 
        array (
          0 => 'src/helpers.php',
        ),
      ),
    ),
  ),
  'vote' => 
  array (
    'id' => 'vote',
    'name' => 'Vote',
    'description' => 'A vote plugin to reward players when they vote.',
    'version' => '0.1.6',
    'url' => 'https://azuriom.com/market/resources/2',
    'authors' => 
    array (
      0 => 'Azuriom',
    ),
    'providers' => 
    array (
      0 => '\\Azuriom\\Plugin\\Vote\\Providers\\VoteServiceProvider',
      1 => '\\Azuriom\\Plugin\\Vote\\Providers\\RouteServiceProvider',
    ),
    'apiId' => 2,
    'composer' => 
    array (
      'name' => 'azuriom/vote',
      'type' => 'azuriom-plugin',
      'description' => 'Vote plugin for Azuriom',
      'license' => 'MIT',
      'autoload' => 
      array (
        'psr-4' => 
        array (
          'Azuriom\\Plugin\\Vote\\' => 'src/',
        ),
        'files' => 
        array (
          0 => 'src/helpers.php',
        ),
      ),
    ),
  ),
);